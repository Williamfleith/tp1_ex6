**Readme Ex6:**

Created on 21/04/2020 by William FLEITH


**Objective :** We imported a class unittest to do a bunch of tests on our function tocheck whether or not it works.

**Pylint :** The programm ex6.py is rated 10/10. But the test.py is only rated 9.62/10 because of the line for the python path taht can't be placed first but it doesn't affect the functionaly of the programm (it is the same in ex4 and 5 and it will be the same until we created a setup.py file for the pythonpath).


**Running :**
 To run the programm go to the package test and run test.py
- The line : sys.path.insert(0, "/home/tp/AGC_TP/tp1_ex6") enable not to setup the PYTHONPATH every time, it will do it automatically everytime we will run the programm.
- Becareful to change this line (line 9) on test/test.py with your absolute right way.
- Files with extension .pyc are removed from the repository.
- Init files are only for the initiation of packages so they are likely to be empty.

**Unittest :**
When you run the programm with unittest it print a point . when the test ran OK and a E when it got an error. 