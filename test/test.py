# -*- coding: utf-8 -*-
"""
Created on 03/04/2020
by William FLEITH
"""
import sys
import unittest

# Insert in the first value of the PythonPath list the PYTHONPATH corresponding to this exercise.
sys.path.insert(0, "/home/tp/AGC_TP/tp1_ex6")
from calculator.ex6 import SimpleCalculator



class Test(unittest.TestCase):
    """
    Class unittest, with 2 methods to do test concerning sum in the first and divide in the second.
    """

    def test_sum(self):
        """
        function test_sum, use the code to do the sum then check if the result is the one we wanted.
        assertEqual is use to check for an expected result.
        Unittest print a . if the test ran OK and a E if it got an error.
        """
        test = SimpleCalculator(2, 1)
        result = test.sum()
        self.assertEqual(result, 3)

        test = SimpleCalculator(2, -1)
        result = test.sum()
        self.assertEqual(result, 1)

        test = SimpleCalculator(-1, 2)
        result = test.sum()
        self.assertEqual(result, 1)

        test = SimpleCalculator(-2, -1)
        result = test.sum()
        self.assertEqual(result, -3)

    def test_divide(self):
        """
        function test_divide to keep from division by 0
        """
        test = SimpleCalculator(2, 1)
        result = test.divide()
        self.assertEqual(result, 2)

        test = SimpleCalculator(2, 0)
        #launching the test with the raise of ZeroDivisionError
        with self.assertRaises(ZeroDivisionError):
            test.divide()

# Main code, call the unittest class.
unittest.main()
